# Pre-implementation Procedure
* Complete steps from [Install Guide](INSTALL.md)
* Ensure each linux host is correctly named using the proper fqdn of the host, using the correct procedure for your operating system resolve any occurrences of incorrect host name. A reboot is required if the host name is altered `hostname -f`
* Log Rotate 
	* update syslog rotation to rotate daily and retain no more than 7 days locally

# Data Acquisition Procedure AIX Operating Systems

Data collection for security and IT operational use cases

## Deployment Server Role "SRV"

* Stage the following apps to deployment-apps - If converting from Stock to best practices patterns fully remove the existing TAs, inputs and server classes. Files not present in the repo might not be replaced causing unexpected results
	* `Splunk_TA_nix_SecKit_sun_0_inputs `
* Install the following apps on the deployment server
	* `SecKit_all_deploymentserver_2_ossun`