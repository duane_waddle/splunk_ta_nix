The Splunk Technology Add-on for Unix and Linux works with the Splunk App for Unix and Linux to provide rapid insights and operational visibility into large-scale Unix and Linux environments. With its new pre-packaged alerting capability, flexible service-based hosts grouping, and easy management of many data sources, it arms administrators with a powerful ability to quickly identify performance and capacity bottlenecks and outliers in Unix and Linux environments.

This guide will cover linux OS only, there are additional pages for additional operating systems


* [Install Guide](INSTALL.md)
* [Install Guide Linux Collection](INSTALL-Linux.md)
* [Install Guide AIX Collection](INSTALL-AIX.md)
* [Install Guide SUN Collection](INSTALL-SUN.md)