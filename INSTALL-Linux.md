# Pre-implementation Procedure
* Complete steps from [Install Guide](INSTALL.md)
* Ensure each linux host is correctly named using the proper fqdn of the host, using the correct procedure for your operating system resolve any occurrences of incorrect host name. A reboot is required if the host name is altered `hostname -f`
* Log Rotate 
	* update syslog rotation to rotate daily and retain no more than 7 days locally
	* Conditional: If Splunk is not running as root ensure the splunk ID can access critical files

``` bash
sudo /usr/bin/setfacl -m "u:splunk:r-x" /var/log
sudo /usr/bin/setfacl -m "u:splunk:r--" /var/log/*
sudo /usr/bin/setfacl -m d:user:splunk:r /var/log
sudo /usr/bin/setfacl -m "u:splunk:r-x" /etc
sudo /usr/bin/setfacl -m "u:splunk:r--" /etc/*
```

# Data Acquisition Procedure Linux Operating Systems

Data collection for security and IT operational use cases

## Server Role Splunk Enterprise Linux
* Deploy to apps the following
	* `Splunk_TA_nix` (Note this step is utilized by more than one nix operating system guide)
	* `Splunk_TA_nix_SecKit_linux_0_shared_script_inputs`
	* `Splunk_TA_nix_SecKit_linux_0_inputs`
	* `Splunk_TA_nix_SecKit_linux_0_etc_monitor_inputs`
	* `Splunk_TA_nix_SecKit_linux_0_rhel_inputs` (select based on os)
	* `Splunk_TA_nix_SecKit_linux_0_ubuntu_inputs` (select based on os)
	* `Splunk_TA_nix_SecKit_linux_1_inputs`
	* `Splunk_TA_nix_SecKit_linux_performance_1_inputs` Collects OS performance information for ITSI 
	* One of the index definition app from src/index within the repository based on the system storage model

## Deployment Server Role "SRV"

* Stage the following apps to deployment-apps - If converting from Stock to best practices patterns fully remove the existing TAs, inputs and server classes. Files not present in the repo might not be replaced causing unexpected results
	* `Splunk_TA_nix_SecKit_linux_0_shared_script_inputs`
	* `Splunk_TA_nix_SecKit_linux_0_inputs`
	* `Splunk_TA_nix_SecKit_linux_0_etc_monitor_inputs`
	* `Splunk_TA_nix_SecKit_linux_0_rhel_inputs`
	* `Splunk_TA_nix_SecKit_linux_0_ubuntu_inputs`
	* `Splunk_TA_nix_SecKit_linux_1_inputs`

* Install the following apps on the deployment server
	* `SecKit_all_deploymentserver_2_oslinux`
* Update `SecKit_all_deploymentserver_2_oslinux/local/serverclass.conf` define the whitelist.0 to capture all hosts where more complete logging should be applied. In most cases this should apply to all servers. 

``` text
[seckit_all_2_os_linux_1]
whitelist.0= ^- 
```

* Update `SecKit_all_deploymentserver_2_oslinux/local/serverclass.conf` define the whitelist.0 to capture all using operating systems derived from RedHat 

``` text
[seckit_all_2_os_linux_rhel_0]
whitelist.0= ^- 
```

* Update `SecKit_all_deploymentserver_2_oslinux/local/serverclass.conf` define the whitelist.0 to capture all using operating systems derived from Ubuntu 

```text 
[seckit_all_2_os_linux_ubuntu_0] 
whitelist.0= ^- 
```
