# Pre-implementation Procedure

## Server Role Search head
* Deploy to apps the following to all search heads
	* `Splunk_TA_nix` (Note this step is utilized by more than one nix operating system guide)
	* Review `Splunk_TA_nix/default/eventtypes.conf` copy to local and refine index names if required.
	* One of the index definition app from src/index within the repository based on the system storage model
* Deploy the following to all search heads with the Splunk_SA_CIM or the Splunk app for nix
	* `Splunk_SA_nix_CIM`
* Deploy the following to all search heads with the Splunk app for nix
	* `Splunk_SA_nix_app`	 

## Server Role Splunk Cluster Master
* Deploy to master-apps
	* `Splunk_TA_nix` 
	* One of the index definition app from src/index within the repository based on the system storage model
	 
## Deployment Server Role "SRV"

* Indexer role (Via Cluster Master) AND Intermediate heavy forwarders
	* Deploy ``Splunk_TA_windows``  
* Stage the following apps to deployment-apps - If converting from Stock to best practices patterns fully remove the existing TAs, inputs and server classes. Files not present in the repo might not be replaced causing unexpected results
	* `Splunk_TA_nix` (Note this step is utilized by more than one nix operating system guide)

* Install the following apps on the deployment server
	* `SecKit_all_deploymentserver_2_osnix` (Note this step is utilized by more than one nix operating system guide)
